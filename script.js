

console.log(fetch("https://jsonplaceholder.typicode.com/posts"))


fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "GET"
})

.then(response => response.json())
.then(data => {
	console.log(data)
})



async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json)

}

fetchData()


fetch("https://jsonplaceholder.typicode.com/posts", {
method:"POST",
headers:{
	"Content-Type": 'application/json'
},

body: JSON.stringify({
	title: "New Post",
	body: "Hello World",
	userID: 1
})
})

.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method:"PUT",
	headers:{
		"Content-Type":"application/json"
	
	},

body: JSON.stringify({
	
	id: 1,
	title: "Update Post",
	body: "Hello Again",
	userID: 1
})
})

.then(res => res.json())
.then(data => console.log(data))
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));



fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data => {
	console.log(data)
})


async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	
	let json = await result.json();

	console.log(json);

}

fetchData()


fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated post"
	})
})
.then(res => res.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))



fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))






fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))

